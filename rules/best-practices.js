module.exports = {
	env: {node: true, es6: true, browser: true, jquery: true, jest: true},
	extends: ['eslint:recommended', 'prettier'],
	plugins: ['prettier', 'file-progress'],
	parser: '@babel/eslint-parser',
	parserOptions: {
		ecmaVersion: 2018,
		sourceType: 'module',
		requireConfigFile: false,
		ecmaFeatures: {
			jsx: true
		},
		babelOptions: {
			plugins: [
				'@babel/plugin-proposal-class-properties',
				'@babel/plugin-syntax-class-properties',
				'@babel/plugin-syntax-jsx'
			]
		}
	},
	settings: {
		progress: {
			successMessage: 'Done..!'
		}
	},
	rules: {
		'prettier/prettier': [
			'error',
			{
				bracketSpacing: false,
				printWidth: 130,
				singleQuote: true,
				trailingComma: 'none',
				tabWidth: 4,
				semi: false,
				useTabs: true,
				quoteProps: 'consistent'
			}
		],
		'file-progress/activate': 1,
		'max-lines-per-function': [
			1,
			{
				max: 800,
				skipBlankLines: true,
				skipComments: true
			}
		],
		'quote-props': ['error', 'consistent-as-needed'],
		'quotes': ['error', 'single'],
		'default-case': ['error', {commentPattern: '^no\\sdefault'}],
		'comma-dangle': [
			'error',
			{
				arrays: 'never',
				objects: 'never',
				imports: 'never',
				exports: 'never',
				functions: 'never'
			}
		],
		'no-shadow': [
			'warn',
			{builtinGlobals: true, hoist: 'all', allow: ['resolve', 'reject', 'done', 'cb'], ignoreOnInitialization: true}
		],
		'no-unused-vars': ['error', {vars: 'all', args: 'none'}],
		'global-require': 'error',
		'arrow-parens': ['error', 'always'],
		'semi': ['error', 'never'],
		'newline-per-chained-call': ['error', {ignoreChainWithDepth: 3}],
		'yoda': 'error'
	}
}

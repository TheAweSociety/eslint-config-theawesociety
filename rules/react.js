const {rules, settings, env, parser, parserOptions, extends: ext, plugins} = require('./best-practices')

module.exports = {
	env: Object.assign(env, {
		browser: true
	}),
	parser,
	parserOptions,
	plugins: [...plugins, 'react', 'react-hooks', 'babel'],
	extends: [...ext, 'plugin:react/recommended'],
	rules: Object.assign(rules, {
		'no-invalid-this': 'off',
		'react-hooks/rules-of-hooks': 'error',
		'react-hooks/exhaustive-deps': 'warn',
		'babel/no-invalid-this': 'warn'
	}),
	settings: Object.assign(settings, {
		react: {
			version: 'detect'
		}
	})
}
